# deno-hill

**deno-hill** is a port of node-hill imported to Deno.

**This repository is archived due to the Brick Hill shutdown announce, you can find [more information](https://blog.brick-hill.com/reducing-services/) right here.**

## Plannings
- [ ] deno-hill as a official deno package

## Installation

To install `deno-hill`, you need to have Deno installed on your system. You can install Deno by following the instructions at [https://deno.land/](https://deno.land/).

Once Deno is installed, clone the repository or take a release from relases

Then run `launch.bat` or run this command (they are the same but anyway):

```
deno run --allow-read --allow-net .\start.js
```

## Why i should use deno-hill instead of node-hill

### The only reason: Deno > Node

1. **Secure by default**: Deno has security features built into its core, such as explicit permissions and a sandboxed environment. By default, Deno restricts access to the file system, network, and environment variables, preventing unauthorized access.

2. **Built-in TypeScript support**: Deno natively supports TypeScript without the need for additional configuration or transpilation steps. This makes it easier to write and maintain type-safe code, enhancing code quality and developer productivity.

3. **Module system**: Deno uses modern JavaScript module syntax (ES modules) without the need for a package manager or a package.json file. It allows you to import modules directly from URLs, making dependency management simpler and more intuitive.

4. **Improved developer experience**: Deno provides a set of built-in tools, such as a built-in dependency inspector (`deno info`), a formatter (`deno fmt`), a linter (`deno lint`), and a test runner (`deno test`). These tools help streamline development workflows and reduce the need for external tools or configurations.

5. **Standard library**: Deno comes with a standard library that provides a range of essential utilities, including HTTP, file system, and cryptography APIs. This reduces the need for third-party dependencies and facilitates building applications without relying on external packages.

6. **Better security model**: Deno uses explicit permissions, which means you have to explicitly grant access to resources like the file system or network. This fine-grained control enhances security and reduces the risk of unintended access to sensitive data or operations.

7. **Improved performance**: Deno is built on top of the V8 JavaScript engine and incorporates modern JavaScript runtime techniques. It offers improved performance in certain scenarios and has better support for modern JavaScript language features.

It's important to note that Node.js still has a large and vibrant ecosystem with a vast number of packages and community support. The choice between Deno and Node.js ultimately depends on the specific requirements and preferences of your project.

For more

 information about Deno and its features, you can visit the official Deno website at [https://deno.land/](https://deno.land/).

## License

This module is licensed under the [GPLv3 License](LICENSE).
